const router = require("express").Router();
const { pingController } = require("./../controllers/ping.controller");

router.get("/", pingController);

module.exports = router;
