const express = require("express");
const cors = require("cors");

const pingRoute = require("./routes/ping");

const app = express();

app.use(cors());
app.use(express.json());

app.get("/", (req, res) => {
  res.send("<h1>Hello World!</h1>");
});

app.use("/api/ping", pingRoute);

const PORT = process.env.PORT || 7000;

app.listen(PORT, () => {
  console.log(`server listening at http://localhost:${PORT}`);
});
